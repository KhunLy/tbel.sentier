﻿using System;
using System.Collections.Generic;
using Models;
using Models.Concretes;
using Models.Enums;
using Models.Interfaces;

namespace SentierV2
{
    class Program
    {
        static void Main(string[] args)
        {
            PlanteBota plante = new PlanteBota
            {
                NomLatin = "Papaver rhoeas",
                Statuts = new List<EStatut> { EStatut.Protege },
                Coordonnee = new Coordonnee(54, 4),
                Saisons = new List<ESaison> { ESaison.Printemps, ESaison.Ete },
            };
            IAnglais tradEn = ((IAnglais)plante);
            tradEn.Nom = "poppy";
            tradEn.Description = "red flower";

            IFrancais tradFr = plante;
            tradFr.Nom = "coquelicot";
            tradFr.Description = "Fleur rouge";

            Console.WriteLine("Veuillez choisir votre langue");
            string langue = Console.ReadLine();

            if(langue == "en")
            {
                DiplayEn(plante);
            }
            if (langue == "fr")
            {
                DiplayFr(plante);
            }
        }

        static void DiplayEn(IAnglais plante)
        {
            Console.WriteLine($"{plante.Nom}");
            Console.WriteLine($"{plante.Description}");
        }

        static void DiplayFr(IFrancais plante)
        {
            Console.WriteLine($"{plante.Nom}");
            Console.WriteLine($"{plante.Description}");
        }
    }
}
