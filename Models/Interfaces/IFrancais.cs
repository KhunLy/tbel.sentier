﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Interfaces
{
    public interface IFrancais
    {
        string Nom { get; set; }
        string Description { get; set; }
    }
}
