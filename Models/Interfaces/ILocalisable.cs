﻿using Models.Concretes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Interfaces
{
    public interface ILocalisable
    {
        Coordonnee Coordonnee { get; set; }
    }
}
