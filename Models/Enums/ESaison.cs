﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Enums
{
    public enum ESaison
    {
        Printemps, Ete, Automne, Hiver
    }
}
