﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Enums
{
    public enum EStatut
    {
        Protege, Endemique, Menace, Rare
    }
}
