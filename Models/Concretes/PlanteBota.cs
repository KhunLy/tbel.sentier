﻿using Models.Abstraites;
using Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Concretes
{
    public class PlanteBota : Plante
    {
        public List<EVertu> Vertus { get; set; }
        public string NomLatin { get; set; }
        public List<EStatut> Statuts { get; set; }
    }
}
