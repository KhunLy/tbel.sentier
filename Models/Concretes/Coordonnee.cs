﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Concretes
{
    public class Coordonnee
    {
        public Coordonnee(double longitude, double latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        public double Altitude { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
